# bish

**bish** is a modular toolkit for [Bash](https://www.gnu.org/software/bash/).

[![Apache License 2.0](https://img.shields.io/static/v1?label=License&message=Apache%20License%202.0&style=flat-square&color=informational&logo=apache)](https://www.apache.org/licenses/LICENSE-2.0)
[![Semantic Versioning 2.0.0](https://img.shields.io/static/v1?label=Semantic%20Versioning&message=2.0.0&style=flat-square&color=informational)](https://semver.org/spec/v2.0.0)
[![Keep a Changelog 1.0.0](https://img.shields.io/static/v1?label=Keep%20a%20Changelog&message=1.0.0&style=flat-square&color=informational)](https://keepachangelog.com/en/1.0.0)
[![Hosted on GitLab](https://img.shields.io/static/v1?label=Hosted%20on&message=GitLab&style=flat-square&color=informational&logo=gitlab)](https://gitlab.com/lpireyn/bish)

## Table of contents

[[_TOC_]]

## Installation

### git clone

bish can be installed anywhere, but we recommend cloning it in the `$HOME/.local/lib/bish` directory.

``` shell
git clone https://gitlab.com/lpireyn/bish.git $HOME/.local/lib/bish
```

## Update

Update bish with:

``` shell
git -C $HOME/.local/lib/bish pull
```
## Usage

### Source bish

To use bish, source `bish` in your Bash script:

``` shell
. $HOME/.local/lib/bish/bish || exit 1
```

The `|| exit 1` ensures your script fails early if bish is not available.

If the actual location of bish is not known in advance (e.g. if you want your script to be portable), you should use the following safer code:

``` shell
. "${BISH_HOME:-$HOME/.local/lib/bish}/bish" || exit 1
```

### Load modules

Once bish has been sourced, you can load the modules you need:

``` shell
b_mod_load color log xdg || exit 1
```

Again, the `|| exit 1` ensures your script fails early if any of the modules cannot be loaded for some reason.

See also [Modules](#modules).

## Modules

bish is based on *modules*.

Modules provide specific functions and can be loaded as needed in your Bash script.
This allows bish to be infinitely extensible whilst maintaining a very fast loading time and a very small memory footprint in the majority of cases.

Modules can load other modules, and bish gracefully handles cycles if there are any.

A few *builtin* modules are necessary for bish's core features.
Those modules cannot be unloaded.

### ansi

The **ansi** module provides a function that helps print *ANSI escape sequences*.

| Function | Synopsis | Description |
| -------- | -------- | ----------- |
| b_ansi_esc | `b_ansi_esc CODE...` | Prints the ANSI escape sequence made of `CODE`s. |

See also [the `color` module](#color).

### cmd

*builtin*

The **cmd** module provides functions that help work with *commands*.

| Function | Synopsis | Description |
| -------- | -------- | ----------- |
| b_cmd_is | `b_cmd_is TYPE COMMAND` | Returns whether `COMMAND` is of type `TYPE` (`TYPE` can be one of `alias`, `builtin`, `file` and `function`). |
| b_cmd_is_alias | `b_cmd_is_alias COMMAND` | Returns whether `COMMAND` is an alias (see `b_cmd_is`). |
| b_cmd_is_builtin | `b_cmd_is_builtin COMMAND` | Returns whether `COMMAND` is a builtin (see `b_cmd_is`). |
| b_cmd_is_file | `b_cmd_is_file COMMAND` | Returns whether `COMMAND` is a file (see `b_cmd_is`). |
| b_cmd_is_function | `b_cmd_is_function COMMAND` | Returns whether `COMMAND` is a function (see `b_cmd_is`). |
| b_cmd_exists | `b_cmd_exists COMMAND` | Returns whether `COMMAND` exists. |
| b_cmd_mute | `b_cmd_mute COMMAND [ARG...]` | Runs and mutes `COMMAND` (stdout and stderr are muted). |
| b_cmd_quiet | `b_cmd_quiet COMMAND [ARG...]` | Runs `COMMAND` quietly (stdout and stderr are muted and the exit value is ignored). |
| b_cmd_require | `b_cmd_require COMMAND...` | Requires commands to be available, and prints an error message otherwise. |

### color

TODO

### editor

The **editor** module provides functions that help invoke an *editor*.

| Function | Synopsis | Description |
| -------- | -------- | ----------- |
| b_editor | `b_editor` | Prints the editor command (`$EDITOR` if set, result of `b_editor_guess` otherwise). |
| b_editor_guess | `b_editor_guess` | Guesses and prints the editor command (first available of `emacs`, `vim`, `vi` and `nano`). |

### fasd

Integration of [fasd](https://github.com/clvv/fasd).

### file

TODO

### fortune

TODO

### help

TODO

### lemonbar

TODO

### list

TODO

### log

TODO

| Setting | Description | Default value |
| ------- | ----------- | ------------- |
| `log.level` | Threshold log level. | `warn` |
| `log.style` | Log style: `word`, `letter` or `smiley`. | `word` |

### meta

TODO

### mod

*builtin*

The **mod** module provides functions to manage *modules*.
This is a builtin module for obvious reasons.

| Function | Synopsis | Description |
| -------- | -------- | ----------- |
| b_mod_dirs | `b_mod_dirs` | Prints the modules directories, i.e. the directories where modules are searched for. |
| b_mod_file | `b_mod_file MODULE` | Prints the file for MODULE, searched for in the modules directories (see `b_mod_dirs`). |
| b_mod_status | `b_mod_status MODULE` | Prints the status of MODULE. |
| b_mod_list | `b_mod_list [-l\|--loaded]` | Prints the modules. The `-l\|--loaded` option limits the list to the loaded modules. |
| b_mod_load | `b_mod_load MODULE...` | Loads modules. |
| b_mod_unload | `b_mod_unload MODULE...` | Unloads modules. |
| b_mod_reload | `b_mod_reload MODULE...` | Reloads modules. |

### ohmygit

Integration of [Oh My Git](https://github.com/arialdomartini/oh-my-git).

This module adds a line to the prompt but is not a standalone prompt.

| Setting | Description | Default value |
| ------- | ----------- | ------------- |
| `ohmygit.file` | Location of the `prompt.sh` file. | `$HOME/.local/lib/ohmygit/prompt.sh` |

### os

The **os** module provides functions to determine the OS.

| Function | Synopsis | Description |
| -------- | -------- | ----------- |
| b_os | `b_os` | Prints the OS. |

### process

TODO

### prompt

TODO

### settings

TODO

### xdg

TODO

### z

Integration of [z](https://github.com/rupa/z/).

| Setting | Description | Default value |
| ------- | ----------- | ------------- |
| `z.file` | Location of the `z.sh` file. | `$HOME/.local/lib/z/z.sh` |

## Prompts

### classic

Commonly used Bash prompt that mentions the username, hostname and working directory.

### default

Default Bash prompt.

### fancy

Fancier version of the *classic* prompt, with colors and exit value.

### liquidprompt

Integration of [Liquidprompt](https://github.com/nojhan/liquidprompt).

| Setting | Description | Default value |
| ------- | ----------- | ------------- |
| `liquidprompt.file` | Location of the `liquidprompt` file. | `$HOME/.local/lib/liquidprompt/liquidprompt` |

### powerlinego

Integration of [Powerline Go](https://github.com/justjanne/powerline-go).

| Setting | Description | Default value |
| ------- | ----------- | ------------- |
| `powerlinego.opts` | Options passed to `powerline-go`. | |

### starship

Integration of [Starship](https://starship.rs/).

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## FAQ

### Why yet another toolkit for Bash?

Unlike a lot of the existing toolkits and libraries for Bash, bish is designed with the following principles:

**Simplicity**

Writing sophisticated Bash scripts is hard, and writing *robust* Bash scripts is even harder.
bish tries to hide that complexity and provides simple-to-use features instead.

**Performance**

Bash scripts often call external commands (`cut`, `grep`, `sed`, `awk`, etc.), but Bash builtins are much more efficient and can replace a lot of those commands.
bish tries to do as much as possible with Bash builtins only.

**Modularity**

When you source `bish`, you only get a minimal set of core variables and functions.
Everything else is defined in specific *modules* that can be loaded to meet the particular requirements of each script.
Furthermore, custom modules are easy to write and can even be contributed to bish.
This modularity contributes to the simplicity and performance principles.

### Why is it called *bish*?

According to [dictionary.com](https://www.dictionary.com/browse/bish):

> **bish** / (bɪʃ) /<br>
> *noun*<br>
> 1. British *slang* a mistake

Oh, and it sounds a bit like *bash*, too. Duh.

## License

bish is licensed under the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) ([local copy](LICENSE)).

## Further reading

The following websites are helpful sources of inspiration:

- [Greg's Wiki](https://mywiki.wooledge.org/)
- [pure bash bible](https://github.com/dylanaraps/pure-bash-bible)
