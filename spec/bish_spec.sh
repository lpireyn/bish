# -*- mode: sh; sh-shell: sh -*-
# shellcheck shell=sh
# shellcheck disable=SC2164,SC2287,SC2288
# ------------------------------------------------------------------------------
# bish Shellspec spec
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

Describe 'bish'
    Include bish
    Describe 'Module: ansi'
        Before 'b_mod_load ansi'
        Pending TODO
    End
    Describe 'Module: cmd'
        Describe 'b_cmd_is'
            Parameters
                builtin cd 0
                file ls 0
                'function' b_cmd_is 0
                alias b_cmd_is 1
                builtin ls 1
                file b_cmd_is 1
                'function' ls 1
            End
            Example "b_cmd_is $1 $2"
                When call b_cmd_is "$1" "$2"
                The status should eq "$3"
            End
        End
        Describe 'b_cmd_is_alias'
            Parameters
                b_cmd_is 1
            End
            Example "b_cmd_is_alias $1"
                When call b_cmd_is_alias "$1"
                The status should eq "$2"
            End
        End
        Describe 'b_cmd_is_builtin'
            Parameters
                cd 0
                b_cmd_is 1
            End
            Example "b_cmd_is_builtin $1"
                When call b_cmd_is_builtin "$1"
                The status should eq "$2"
            End
        End
        Describe 'b_cmd_is_file'
            Parameters
                bash 0
                cd 1
                b_cmd_is 1
            End
            Example "b_cmd_is_file $1"
                When call b_cmd_is_file "$1"
                The status should eq "$2"
            End
        End
        Describe 'b_cmd_is_function'
            Parameters
                b_cmd_is 0
                cd 1
                bash 1
            End
            Example "b_cmd_is_function $1"
                When call b_cmd_is_function "$1"
                The status should eq "$2"
            End
        End
        Describe 'b_cmd_exists'
            Parameters
                b_cmd_is 0
                cd 0
                bash 0
                _this_does_not_exist_ 1
            End
            Example "b_cmd_exists $1"
                When call b_cmd_exists "$1"
                The status should eq "$2"
            End
        End
        Describe 'b_cmd_quiet'
            loud() {
                echo 'scream'
                echo 'scream' >&2
                return 1
            }
            Example 'b_cmd_quiet'
                When call b_cmd_quiet loud
                The status should be success
                The stdout should eq ''
                The stderr should eq ''
            End
        End
    End
    Describe 'Module: color'
        Before 'b_mod_load color'
        Pending TODO
    End
    Describe 'Module: file'
        Describe 'b_file_base'
            Parameters
                'a'
                'a/'
                'a/.'
                'a/b'
                'a/b/'
                'a/b/.'
                '/a'
                '/a/'
                '/a/.'
                '/a/b'
                '/a/b/'
                '/a/b/.'
            End
            Example "b_file_base $1"
                When call b_file_base "$1"
                The stdout should eq "$(basename "$1")"
            End
        End
        Describe 'b_file_dir'
            Parameters
                'a'
                'a/'
                'a/.'
                'a/b'
                'a/b/'
                'a/b/.'
                '/a'
                '/a/'
                '/a/.'
                '/a/b'
                '/a/b/'
                '/a/b/.'
            End
            Example "b_file_dir $1"
                When call b_file_dir "$1"
                The stdout should eq "$(dirname "$1")"
            End
        End
    End
    Describe 'Module: help'
        Pending TODO
    End
    Describe 'Module: list'
        Pending TODO
    End
    Describe 'Module: log'
        Before 'b_mod_load log'
        Pending TODO
    End
    Describe 'Module: meta'
        Pending TODO
    End
    Describe 'Module: mod'
        Pending TODO
    End
    Describe 'Module: path'
        Before 'b_mod_load path'
        Describe 'b_path_split'
            Example 'Empty path'
                When call b_path_split ''
                The stdout should eq ''
            End
            Example 'Path with 1 directory'
                When call b_path_split 'abc'
                The stdout should eq 'abc'
            End
            Example 'Path with 2 directories'
                When call b_path_split 'abc:def'
                The stdout line 1 should eq 'abc'
                The stdout line 2 should eq 'def'
            End
            Example 'Path with 3 directories'
                When call b_path_split 'abc:def:ghi'
                The stdout line 1 should eq 'abc'
                The stdout line 2 should eq 'def'
                The stdout line 3 should eq 'ghi'
            End
        End
        Describe 'b_path_contains'
            Example 'Empty path'
                When call b_path_contains abc ''
                The status should be failure
            End
            Example 'Path with 1 directory, present'
                When call b_path_contains abc abc
                The status should be success
            End
            Example 'Path with 1 directory, absent'
                When call b_path_contains def abc
                The status should be failure
            End
            Example 'Path with 2 directories, present, 1'
                When call b_path_contains abc abc:def
                The status should be success
            End
            Example 'Path with 2 directories, present, 2'
                When call b_path_contains abc def:abc
                The status should be success
            End
            Example 'Path with 2 directories, absent'
                When call b_path_contains abc ab:c
                The status should be failure
            End
            Example 'Path with 3 directories, present, 1'
                When call b_path_contains abc abc:def:ghi
                The status should be success
            End
            Example 'Path with 3 directories, present, 2'
                When call b_path_contains def abc:def:ghi
                The status should be success
            End
            Example 'Path with 3 directories, present, 3'
                When call b_path_contains ghi abc:def:ghi
                The status should be success
            End
            Example 'Path with 3 directories, absent'
                When call b_path_contains abc ab:c:def
                The status should be failure
            End
        End
        Describe 'b_path_prepend'
            Example 'Empty path'
                When call b_path_prepend abc ''
                The stdout should eq abc
            End
            Example 'Path with 1 directory, absent'
                When call b_path_prepend abc def
                The stdout should eq abc:def
            End
            Example 'Path with 1 directory, present'
                When call b_path_prepend abc abc
                The stdout should eq abc
            End
            Example 'Path with 2 directories, absent'
                When call b_path_prepend abc def:ghi
                The stdout should eq abc:def:ghi
            End
            Example 'Path with 2 directories, present, 1'
                When call b_path_prepend abc abc:def
                The stdout should eq abc:def
            End
            Example 'Path with 2 directories, present, 2'
                When call b_path_prepend def abc:def
                The stdout should eq abc:def
            End
        End
        Describe 'b_path_append'
            Example 'Empty path'
                When call b_path_append abc ''
                The stdout should eq abc
            End
            Example 'Path with 1 directory, absent'
                When call b_path_append abc def
                The stdout should eq def:abc
            End
            Example 'Path with 1 directory, present'
                When call b_path_append abc abc
                The stdout should eq abc
            End
            Example 'Path with 2 directories, absent'
                When call b_path_append abc def:ghi
                The stdout should eq def:ghi:abc
            End
            Example 'Path with 2 directories, present, 1'
                When call b_path_append abc abc:def
                The stdout should eq abc:def
            End
            Example 'Path with 2 directories, present, 2'
                When call b_path_append def abc:def
                The stdout should eq abc:def
            End
        End
        Describe 'b_path_remove'
            Example 'Empty path'
                When call b_path_remove abc ''
                The stdout should eq ''
            End
            Example 'Path with 1 directory, present'
                When call b_path_remove abc abc
                The stdout should eq ''
            End
            Example 'Path with 1 directory, absent'
                When call b_path_remove abc def
                The stdout should eq def
            End
            Example 'Path with 2 directories, present, 1'
                When call b_path_remove abc abc:def
                The stdout should eq def
            End
            Example 'Path with 2 directories, present, 2'
                When call b_path_remove def abc:def
                The stdout should eq abc
            End
            Example 'Path with 2 directories, absent'
                When call b_path_remove abc def:ghi
                The stdout should eq def:ghi
            End
        End
    End
    Describe 'Module: process'
        Before 'b_mod_load process'
        Pending TODO
    End
    Describe 'Module: settings'
        Before 'b_mod_load settings'
        Pending TODO
    End
    Describe 'Module: xdg'
        Before 'b_mod_load xdg'
        Pending TODO
    End
End
