#!/usr/bin/env sh

shellcheck \
    bish \
    mods/*.bash prompts/*.bash \
    spec/*.sh \
    check.sh
