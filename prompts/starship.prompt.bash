# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish prompt - Starship
# Author: Laurent Pireyn
# See also: https://starship.rs/
# ------------------------------------------------------------------------------

_b_starship_on_load() {
    if ! b_cmd_exists starship; then
        b_error 'Starship not found'
        return 1
    fi
}

_b_starship_prompt_command() {
    eval "$(starship init bash)"
}
