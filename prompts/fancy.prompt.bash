# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish prompt - Fancy
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_fancy_on_load() {
    b_mod_load color
}

_b_fancy_prompt_command() {
    # Obtain last status first
    local -ir last_status=$?
    # Build prompt
    PS1='\['
    if [[ "$EUID" -eq 0 ]]; then
        PS1+="$(b_color fg red)"
    else
        PS1+="$(b_color fg yellow)"
    fi
    PS1+="\]\u\[$(b_color)\]@\[$(b_color fg cyan)\]\h\[$(b_color)\] \[$(b_color fg blue)\]\w\n"
    if [[ $last_status -eq 0 ]]; then
        PS1+="\[$(b_color fg green)\]✔"
    else
        PS1+="\[$(b_color fg red bold)\]✘ $last_status"
    fi
    PS1+=" \[$(b_color fg white)\]❯\[$(b_color)\] "
}
