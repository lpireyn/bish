# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish prompt - Liquidprompt
# Author: Laurent Pireyn
# See also: https://github.com/nojhan/liquidprompt
# ------------------------------------------------------------------------------

_b_liquidprompt_on_load() {
    b_mod_load settings || return 1

    b_settings_register liquidprompt.file "$HOME/.local/lib/liquidprompt/liquidprompt" 'Liquidprompt file'

    local file
    file="$(b_settings_get liquidprompt.file)"
    if [[ -z "$file" || ! -f "$file" ]]; then
        b_error "Liquidprompt not found ($file)"
    fi
    # shellcheck source=/dev/null
    . "$file"
}

_b_liquidprompt_on_unload() {
    b_prompt_remove_prompt_command _lp_set_prompt
    b_settings_deregister liquidprompt.file
}
