# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish prompt - Powerline Go
# Author: Laurent Pireyn
# See also: https://github.com/justjanne/powerline-go
# ------------------------------------------------------------------------------

_b_powerlinego_on_load() {
    b_mod_load settings || return 1

    if ! b_cmd_exists powerline-go; then
        b_error 'Powerline Go not found'
        return 1
    fi

    b_settings_register powerlinego.opts '' 'Powerline Go options'
}

_b_powerlinego_on_unload() {
    b_settings_deregister powerlinego.opts
}

_b_powerlinego_prompt_command() {
    local -a opts
    mapfile -t opts < <(b_settings_get powerlinego.opts)
    PS1="$(powerline-go -shell bash -error $? "${opts[@]}")"
}
