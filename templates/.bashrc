# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# ~/.bashrc
# ------------------------------------------------------------------------------

# Return if Bash is non interactive
[[ "${-}" != *i* ]] && return 0

# bish
. "${BISH_HOME:-$HOME/.local/lib/bish}"/bish || return 0
b_mod_load log path xdg || return 0

# Prompt

b_prompt_select fancy

# Configuration

_bashrc_source_files() {
    local dir
    local file
    for dir in $(b_path_split "$(b_xdg_config_path)"); do
        for file in "$dir"/bash/bashrc.d/*; do
            [[ -f "$file" ]] || continue
            b_log debug "Sourcing $file"
            . "$file" || return 0
        done
    done
}
_bashrc_source_files
unset -f _bashrc_source_files
