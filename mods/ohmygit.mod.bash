# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - Oh My Git
# Author: Laurent Pireyn
# See also: https://github.com/arialdomartini/oh-my-git
# ------------------------------------------------------------------------------

_b_ohmygit_on_load() {
    b_mod_load log prompt settings || return 1

    _b_ohmygit_prompt_command() {
        # Reset some Oh My Git variables to the current prompt
        declare -gx omg_second_line="$PS1"
        declare -gx omg_ungit_prompt="$PS1"
        # Let Oh My Git augment the current prompt
        bash_prompt
    }

    b_settings_register ohmygit.file "$HOME/.local/lib/oh-my-git/prompt.sh" 'Oh My Git file'

    local file
    file="$(b_settings_get ohmygit.file)"
    if [[ -z "$file" || ! -f "$file" ]]; then
        b_error "Oh My Git not found ($file)"
        return 1
    fi
    # Load Oh My Git but restore PROMPT_COMMAND
    local -r old_prompt_command="$PROMPT_COMMAND"
    # shellcheck source=/dev/null
    . "$file"
    PROMPT_COMMAND="$old_prompt_command"
    # Add custom prompt command instead
    b_prompt_append_prompt_command _b_ohmygit_prompt_command
}

_b_ohmygit_on_unload() {
    b_prompt_remove_prompt_command _b_ohmygit_prompt_command
    b_settings_deregister ohmygit.file
    unset -f _b_ohmygit_prompt_command
    b_log warn 'The ohmygit module cannot be completely unloaded'
}
