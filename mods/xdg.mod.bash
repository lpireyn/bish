# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - XDG
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_xdg_on_load() {
    b_mod_load path

    b_xdg_data_home() {
        help_summary 'print the XDG data home'
        printf '%s\n' "${XDG_DATA_HOME:-$HOME/.local/share}"
    }

    b_xdg_state_home() {
        help_summary 'print the XDG state home'
        printf '%s\n' "${XDG_STATE_HOME:-$HOME/.local/state}"
    }

    b_xdg_config_home() {
        help_summary 'print the XDG config home'
        printf '%s\n' "${XDG_CONFIG_HOME:-$HOME/.config}"
    }

    b_xdg_cache_home() {
        help_summary 'print the XDG cache home'
        printf '%s\n' "${XDG_CACHE_HOME:-$HOME/.cache}"
    }

    b_xdg_data_dirs() {
        help_summary 'print the XDG data dirs'
        printf '%s\n' "${XDG_DATA_DIRS:-/usr/local/share:/usr/share}"
    }

    b_xdg_config_dirs() {
        help_summary 'print the XDG config dirs'
        printf '%s\n' "${XDG_CONFIG_DIRS:-/etc/xdg}"
    }

    b_xdg_data_path() {
        b_path_prepend "$(b_xdg_data_home)" "$(b_xdg_data_dirs)"
    }

    b_xdg_config_path() {
        b_path_prepend "$(b_xdg_config_home)" "$(b_xdg_config_dirs)"
    }

    b_xdg_data_file() {
        help_summary 'print the first data file found in the XDG data dirs'
        help_synopsis 'file'
        b_file_in_dirs "$(b_xdg_data_path)" "$@"
    }

    b_xdg_config_file() {
        help_summary 'print the first config file found in the XDG config dirs'
        help_synopsis 'file'
        b_file_in_dirs "$(b_xdg_config_path)" "$@"
    }

    b_xdg_runtime_dir() {
        help_summary 'print the XDG runtime dir'
        printf '%s\n' "${XDG_RUNTIME_DIR:-/tmp/$UID}"
    }
}

_b_xdg_on_unload() {
    unset -f b_xdg_{data_home,config_home,cache_home,data_dirs,config_dirs,data_path,config_path,data_file,config_file,runtime_dir}
}
