# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - LemonBar
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_lemonbar_on_load() {
    b_lemonbar_format() {
        local -r prefix="$1"
        local -r suffix="$2"
        local -r text="$3"
        if [[ -n "$prefix" ]]; then
            printf '%%{%s}%s%%{%s}' "$prefix" "$text" "$suffix"
        else
            printf '%s' "$text"
        fi
    }

    b_lemonbar_color() {
        local -r type="$1"
        b_require_arg type "$type" || return 2
        local -r value="$2"
        local -r text="$3"
        b_lemonbar_format "${value:+${type}${value}}" "${type}-" "$text"
    }

    b_lemonbar_bg_color() {
        local -r value="$1"
        local -r text="$2"
        b_lemonbar_color B "$value" "$text"
    }

    b_lemonbar_fg_color() {
        local -r value="$1"
        local -r text="$2"
        b_lemonbar_color F "$value" "$text"
    }

    b_lemonbar_colors() {
        local -r bg_color="$1"
        local -r fg_color="$2"
        local -r text="$3"
        local result="$text"
        result="$(b_lemonbar_bg_color "$bg_color" "$result")"
        result="$(b_lemonbar_fg_color "$fg_color" "$result")"
        printf '%s' "$result"
    }

    b_lemonbar_monitor() {
        local -r monitor="$1"
        b_require_arg monitor "$monitor" || return 2
        printf '%%{S%s}' "$monitor"
    }

    b_lemonbar_action() {
        local -r button="$1"
        b_require_arg button "$button" || return 2
        local -r action="$2"
        local -r text="$3"
        if [[ -n "$text" ]]; then
            b_lemonbar_format "${action:+A$button:$action:}" A "$text"
        fi
    }

    b_lemonbar_actions() {
        local -r text="$1"
        local -i button=1
        local result="$text"
        local action
        while [[ $button -le 5 ]]; do
            shift
            action="$1"
            result="$(b_lemonbar_action $button "$action" "$result")"
            (( ++button ))
        done
        printf '%s' "$result"
    }
}

_b_lemonbar_on_unload() {
    unset -f b_lemonbar_{format,color,bg_color,fg_color,colors,monitor,action,actions}
}
