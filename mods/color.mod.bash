# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - Color
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_color_on_load() {
    b_mod_load ansi settings

    b_color() {
        help_summary 'print a color ANSI escape sequence'
        help_synopsis 'color...'
        help_description 'COLOR is one of the following:'
        help_description '    b|bright|bold'
        help_description '    d|dim'
        help_description '    i|italic'
        help_description '    u|underline'
        help_description '    blink'
        help_description '    r|reverse'
        help_description '    h|hidden'
        help_description '    fg|bg [b|bright] NAME    ANSI color'
        help_description '    fg8|bg8 CODE             8-bit color'
        help_description '    fg24|bg24 CODE           24-bit color'
        local -a seq=()
        local -i code
        local color
        while [[ -n "$1" ]]; do
            case "$1" in
                # Attributes
                b|bright|bold)
                    seq+=(1)
                    ;;
                d|dim)
                    seq+=(2)
                    ;;
                i|italic)
                    seq+=(3)
                    ;;
                u|underline)
                    seq+=(4)
                    ;;
                blink)
                    seq+=(5)
                    ;;
                r|reverse)
                    seq+=(7)
                    ;;
                h|hidden)
                    seq+=(8)
                    ;;
                # 4-bit colors
                fg|bg)
                    case "$1" in
                        fg)
                            code=0
                            ;;
                        bg)
                            code=10
                            ;;
                    esac
                    shift
                    case "$1" in
                        b|bright)
                            ((code += 60))
                            shift
                            ;;
                    esac
                    color="${_b_color_colors[$1]}"
                    if [[ -n "$color" ]]; then
                        ((code += color))
                        seq+=("$code")
                    fi
                    ;;
                # 8-bit colors
                fg8|bg8)
                    case "$1" in
                        fg8)
                            code=38
                            ;;
                        bg8)
                            code=48
                            ;;
                    esac
                    shift
                    color="$1"
                    if [[ -n "$color" ]]; then
                        seq+=("$code" 5 "$color")
                    fi
                    ;;
                # 24-bit colors
                fg24|bg24)
                    case "$1" in
                        fg24)
                            code=38
                            ;;
                        bg24)
                            code=48
                            ;;
                    esac
                    shift
                    color="$1"
                    if [[ -n "$color" ]]; then
                        local -a elems
                        mapfile -t elems < <(b_list_split ',' "$color")
                        seq+=("$code" 2 "${elems[@]}")
                    fi
                    ;;
            esac
            shift
        done
        [[ ${#seq[@]} -gt 0 ]] || seq=(0)
        [[ "$(b_settings_get color.disabled)" -eq 0 ]] && return 0
        b_ansi_esc "${seq[@]}"
    }

    declare -Ag _b_color_colors=(
        [black]=30
        [red]=31
        [green]=32
        [yellow]=33
        [blue]=34
        [magenta]=35
        [cyan]=36
        [white]=37
    )

    b_settings_register color.disabled 1 '0 to disable color ANSI escape sequences'

    # TODO: Completion for b_color
}

_b_color_on_unload() {
    b_settings_deregister color.disabled
    unset -f b_color
    unset -v _b_color_colors
}
