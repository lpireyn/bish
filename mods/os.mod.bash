# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - OS
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_os_on_load() {
    b_mod_load log

    b_os() {
        help_summary 'print the OS'
        printf '%s\n' "$_b_os_os"
    }

    declare -g _b_os_os
    case "$OSTYPE" in
        linux*)
            _b_os_os=linux
            ;;
        freebsd*)
            _b_os_os=freebsd
            ;;
        darwin*)
            _b_os_os=macos
            ;;
        cygwin)
            _b_os_os=cygwin
            ;;
        msys)
            _b_os_os=msys
            ;;
        *)
            _b_os_os=unknown
            ;;
    esac
    b_log debug "OS: $_b_os_os"
}

_b_os_on_unload() {
    unset -f b_os
    unset -v _b_os_os
}
