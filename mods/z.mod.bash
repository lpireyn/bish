# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - z
# Author: Laurent Pireyn
# See also: https://github.com/rupa/z/
# ------------------------------------------------------------------------------

_b_z_on_load() {
    b_mod_load log prompt settings xdg || return 1

    _b_z_prompt_command() {
        (_z --add "$(command pwd -P 2>/dev/null)" 2>/dev/null &)
    }

    b_settings_register z.file "$HOME/.local/lib/z/z.sh" 'z file'

    local file
    file="$(b_settings_get z.file)"
    if [[ -z "$file" || ! -f "$file" ]]; then
        b_error "z not found ($file)"
        return 1
    fi
    # Make z more XDG compliant
    declare -gx _Z_DATA
    _Z_DATA="$(b_xdg_cache_home)/z"
    # Load z but don't change PROMPT_COMMAND
    declare -gx _Z_NO_PROMPT_COMMAND=1
    # shellcheck source=/dev/null
    . "$file"
    # Add custom prompt command instead
    b_prompt_append_prompt_command _b_z_prompt_command
}

_b_z_on_unload() {
    b_prompt_remove_prompt_command _b_z_prompt_command
    b_settings_deregister z.file
    unset -f _b_z_prompt_command
    b_log warn 'The z module cannot be completely unloaded'
}
