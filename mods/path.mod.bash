# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - Path
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_path_on_load() {
    b_path_create() {
        help_summary 'create a path'
        help_synopsis '[dir...]'
        b_list_create ':' "@"
    }

    b_path_split() {
        help_summary 'print the directories in a path'
        help_synopsis 'path'
        local -r path="$1"
        b_list_split ':' "$path"
    }

    b_path_contains() {
        help_summary 'return whether a path contains a directory'
        help_synopsis 'dir path'
        local -r dir="$1"
        local -r path="$2"
        b_list_contains ':' "$dir" "$path"
    }

    b_path_prepend() {
        help_summary 'prepend a directory to a path'
        help_synopsis 'dir path'
        local -r dir="$1"
        local -r path="$2"
        b_list_prepend_if_absent ':' "$dir" "$path"
    }

    b_path_append() {
        help_summary 'append a directory to a path'
        help_synopsis 'dir path'
        local -r dir="$1"
        local -r path="$2"
        b_list_append_if_absent ':' "$dir" "$path"
    }

    b_path_remove() {
        help_summary 'remove a directory from a path'
        help_synopsis 'dir path'
        local -r dir="$1"
        local -r path="$2"
        b_list_remove ':' "$dir" "$path"
    }
}

_b_path_on_unload() {
    unset -f b_path_{split,contains,prepend,append,remove}
}
