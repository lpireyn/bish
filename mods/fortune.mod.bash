# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - Fortune
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_fortune_on_load() {
    b_mod_load log settings

    b_fortune() {
        help_summary 'print fortune'
        local -a opts
        mapfile -t opts < <(b_settings_get fortune.opts)
        b_log debug "Executing: fortune ${opts[*]}"
        fortune "${opts[@]}"
    }

    b_settings_register fortune.opts '' 'Fortune options'
}

_b_fortune_on_unload() {
    b_settings_deregister fortune.opts
    unset -f b_fortune
}
