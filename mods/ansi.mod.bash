# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - ANSI
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_ansi_on_load() {
    b_ansi_esc() {
        help_summary 'print an ANSI escape sequence'
        help_synopsis 'sequence'
        b_require_arg seq "$1"
        # TODO: Use string join function
        local seq
        seq="$(printf '%s;' "$@")"
        printf '%s' "\033[${seq%;}m"
    }
}

_b_ansi_on_unload() {
    unset -f b_ansi_esc
}
