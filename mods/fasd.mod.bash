# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - fasd
# Author: Laurent Pireyn
# See also: https://github.com/clvv/fasd
# ------------------------------------------------------------------------------

_b_fasd_on_load() {
    b_mod_load log prompt || return 1

    if ! b_cmd_exists fasd; then
        b_error 'fasd not found'
        return 1
    fi

    # Load fasd but restore PROMPT_COMMAND
    local -r old_prompt_command="$PROMPT_COMMAND"
    eval "$(fasd --init bash-hook bash-ccomp bash-ccomp-install posix-alias)"
    PROMPT_COMMAND="$old_prompt_command"
    # Properly add prompt command instead
    b_prompt_append_prompt_command _fasd_prompt_func
}

_b_fasd_on_unload() {
    b_prompt_remove_prompt_command _fasd_prompt_func
    unset -f _fasd_prompt_func
    b_log warn 'The fasd module cannot be completely unloaded'
}
