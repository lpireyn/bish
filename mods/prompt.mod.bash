# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - Prompt
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_prompt_on_load() {
    b_mod_load log || return 1

    b_prompt_list() {
        help_summary 'print available prompts'
        local file
        for file in "$_b_prompt_prompts_dir"/*.prompt.bash; do
            [[ -f "$file" ]] || continue
            printf '%s\n' "$(b_file_base "${file%.prompt.bash}")"
        done
    }

    b_prompt_current() {
        help_summary 'print currently selected prompt'
        printf '%s\n' "$_b_prompt_current"
    }

    b_prompt_select() {
        help_summary 'select a prompt'
        help_synopsis 'prompt'
        local -r prompt="$1"
        b_require_arg prompt "$prompt" || return 2
        if [[ "$TERM" == dumb ]]; then
            b_log debug "Ignoring prompt on terminal '$TERM'"
            return 0
        fi
        _b_prompt_unload
        _b_prompt_load "$prompt"
    }

    b_prompt_prepend_prompt_command() {
        # shellcheck disable=SC2016
        help_summary 'prepend command to $PROMPT_COMMAND'
        help_synopsis 'cmd'
        local -r cmd="$1"
        b_require_arg cmd "$cmd" || return 2
        PROMPT_COMMAND="$(b_list_prepend_if_absent ';' "$cmd" "$PROMPT_COMMAND")"
    }

    b_prompt_append_prompt_command() {
        # shellcheck disable=SC2016
        help_summary 'append command to $PROMPT_COMMAND'
        help_synopsis 'cmd'
        local -r cmd="$1"
        b_require_arg cmd "$cmd" || return 2
        PROMPT_COMMAND="$(b_list_append_if_absent ';' "$cmd" "$PROMPT_COMMAND")"
    }

    b_prompt_remove_prompt_command() {
        # shellcheck disable=SC2016
        help_summary 'remove command from $PROMPT_COMMAND'
        help_synopsis 'cmd'
        local -r cmd="$1"
        b_require_arg cmd "$cmd" || return 2
        PROMPT_COMMAND="$(b_list_remove ';' "$cmd" "$PROMPT_COMMAND")"
    }

    _b_prompt_load() {
        local -r prompt="$1"
        local -r file="$_b_prompt_prompts_dir"/"$prompt".prompt.bash
        if [[ ! -f "$file" ]]; then
            b_error "Prompt not found: $prompt ($file)"
            return 1
        fi
        b_log debug "Loading prompt '$prompt'"
        # shellcheck source=/dev/null
        if ! . "$file"; then
            b_error "Error loading prompt '$prompt'"
            return 1
        fi
        local -r on_load_fn=_b_"$prompt"_on_load
        if b_cmd_is_function "$on_load_fn"; then
            if ! "$on_load_fn"; then
                b_error "Error in prompt '$prompt' load function '$on_load_fn'"
                return 1
            fi
        fi
        local -r prompt_command_fn=_b_"$prompt"_prompt_command
        local -r ps1_fn=_b_"$prompt"_ps1
        if b_cmd_is_function "$prompt_command_fn"; then
            b_prompt_prepend_prompt_command "$prompt_command_fn"
            PS1=
        elif b_cmd_is_function "$ps1_fn"; then
            if ! PS1="$($ps1_fn)"; then
                b_error "Error in PS1 function $ps1_fn"
                return 1
            fi
        fi
        _b_prompt_current="$prompt"
        b_log debug "PROMPT_COMMAND: $PROMPT_COMMAND"
        b_log debug "PS1: $PS1"
    }

    _b_prompt_unload() {
        [[ -z "$_b_prompt_current" ]] && return 0
        b_log debug "Unloading prompt '$_b_prompt_current'"
        local -r prompt_command_fn=_b_"$_b_prompt_current"_prompt_command
        b_cmd_is_function "$prompt_command_fn" && b_prompt_remove_prompt_command "$prompt_command_fn"
        local -r on_unload_fn=_b_"$_b_prompt_current"_on_unload
        if b_cmd_is_function "$on_unload_fn"; then
            if ! "$on_unload_fn"; then
                b_log error "Error in prompt '$_b_prompt_current' unload function '$on_unload_fn'"
            fi
        fi
        _b_prompt_current=
    }

    _b_complete_prompt_1() {
        local -r word="$2"
        mapfile -t COMPREPLY < <(compgen -W "$(b_prompt_list)" -- "$word")
    }

    declare -g _b_prompt_prompts_dir="$BISH_HOME/prompts"
    declare -g _b_prompt_current=

    complete -F _b_complete_prompt_1 b_prompt_select
}

_b_prompt_on_unload() {
    complete -r b_prompt_select
    unset -f \
        b_prompt_{list,select,prepend_prompt_command,append_prompt_command,remove_prompt_command} \
        _b_prompt_{load,unload} \
        _b_complete_prompt_1
    unset -v _b_prompt_{prompts_dir,current}
}
