# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - Process
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_process_on_load() {
    b_cmd_require sleep pgrep pkill || return 1

    b_process_is_running() {
        help_summary 'return whether a process is running'
        help_synopsis 'process'
        local -r process="$1"
        b_require_arg process "$process"
        pgrep "$process" >/dev/null 2>&1
    }

    b_process_start() {
        help_summary 'start a process in the background'
        help_synopsis 'command'
        local -r cmd="$1"
        b_require_arg cmd "$cmd"
        $cmd &
    }

    b_process_start_single() {
        help_summary 'start a process in the background if it is not already running'
        help_synopsis 'command [process]'
        help_description 'PROCESS defaults to the first word of COMMAND.'
        local -r cmd="$1"
        b_require_arg cmd "$cmd"
        local -r process="${2:-${cmd%% *}}"
        if ! b_process_is_running "$process"; then
            b_process_start "$cmd"
        fi
    }

    b_process_stop() {
        help_summary 'stop a process'
        help_synopsis 'process [signal]'
        help_description 'SIGNAL defaults to "TERM".'
        help_description 'This function waits until PROCESS is not running.'
        local -r process="$1"
        b_require_arg process "$process"
        local -r sig="${2:-TERM}"
        pkill --signal "$sig" "$process"
        while b_process_is_running "$process"; do
            # TODO: Parameterize the time
            sleep 0.2
        done
    }

    b_process_restart() {
        help_summary 'restart a process'
        help_synopsis 'command [process] [signal]'
        help_description 'PROCESS defaults to the first word of COMMAND.'
        help_description 'SIGNAL defaults to "TERM".'
        local -r cmd="$1"
        b_require_arg cmd "$cmd"
        local -r process="${2:-${cmd%% *}}"
        local -r sig="$3"
        b_process_stop "$process" "$sig"
        b_process_start "$cmd"
    }
}

_b_process_on_unload() {
    unset -f b_process_{is_running,start,start_single,stop,restart}
}
