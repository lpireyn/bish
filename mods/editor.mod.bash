# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - Editor
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_editor_on_load() {
    b_editor() {
        help_summary 'print the editor command'
        # shellcheck disable=SC2016
        help_description 'The editor command is "$EDITOR" if it is set, or the result of b_editor_guess.'
        local editor
        if [[ -n "$EDITOR" ]]; then
            editor="$EDITOR"
        else
            editor="$(b_editor_guess)" || return $?
        fi
        printf '%s\n' "$editor"
    }

    b_editor_guess() {
        help_summary 'guess and print the editor command'
        local editor
        for editor in emacs vim vi nano; do
            if b_cmd_exists "$editor"; then
                printf '%s\n' "$editor"
                return 0
            fi
        done
        b_error 'Cannot guess editor command'
        return 1
    }
}

_b_editor_on_unload() {
    unset -f b_editor{,_guess}
}
