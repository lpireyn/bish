# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - Log
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_log_on_load() {
    b_mod_load color settings

    b_log_parse_level() {
        help_summary 'print the log level corresponding to a word'
        help_synopsis 'word'
        local -r word="$1"
        b_require_arg word "$word" || return 2
        local -i level
        case "$word" in
            0|e|error|E|ERROR)
                level=0
                ;;
            1|w|warn|warning|W|WARN|WARNING)
                level=1
                ;;
            2|i|info|I|INFO)
                level=2
                ;;
            3|d|debug|D|DEBUG)
                level=3
                ;;
            *)
                b_error "Unknown log level: $word"
                return 1
                ;;
        esac
        printf '%d\n' $level
    }

    b_log() {
        help_summary 'print a log statement'
        help_synopsis 'level message'
        help_description 'LEVEL is any word supported by b_log_parse_level.'
        local level="$1"
        b_require_arg level "$level" || return 2
        local -r msg="$2"
        level="$(b_log_parse_level "$level")" || return 1
        local threshold
        threshold="$(b_log_parse_level "$(b_settings_get log.level)")" || threshold=1
        [[ "$level" -gt "$threshold" ]] && return 0
        local word
        local smiley
        local -a color
        case "$level" in
            0)
                word=ERROR
                smiley=':-('
                color=(fg bright red)
                ;;
            1)
                word=WARN
                smiley=':-/'
                color=(fg yellow)
                ;;
            2)
                word=INFO
                smiley=':-)'
                color=(fg green)
                ;;
            3)
                word=DEBUG
                smiley='8-^'
                color=(fg blue)
                ;;
            *)
                # Should not happen
                return 10
                ;;
        esac
        local -r style="$(b_settings_get log.style)"
        if [[ "$style" == smiley ]]; then
            printf '%b%s%b %s\n' "$(b_color "${color[@]}")" "$smiley" "$(b_color)" "$msg"
        else
            [[ "$style" == letter ]] && word="${word:0:1}"
            printf '[%b%s%b] %s\n' "$(b_color "${color[@]}")" "$word" "$(b_color)" "$msg"
        fi
    }

    _b_complete_log_level_1() {
        local -r word="$2"
        if [[ "$COMP_CWORD" -eq 1 ]]; then
            mapfile -t COMPREPLY < <(compgen -W '0 e error E ERROR 1 w warn warning W WARN WARNING 2 i info I INFO 3 d debug D DEBUG' -- "$word")
        fi
    }

    complete -F _b_complete_log_level_1 b_log{,_parse_level}

    b_settings_register log.level warn 'Log level'
    b_settings_register log.style word 'Log style: word, letter, smiley'
}

_b_log_on_unload() {
    b_settings_deregister log.{level,style}
    complete -r b_log{,_parse_level}
    unset -f \
        b_log{,_parse_level} \
        _b_complete_log_level_1
}
