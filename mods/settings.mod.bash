# -*- mode: sh; sh-shell: bash -*-
# ------------------------------------------------------------------------------
# bish module - Settings
# Author: Laurent Pireyn
# ------------------------------------------------------------------------------

_b_settings_on_load() {
    b_mod_load xdg

    b_settings_info() {
        help_summary 'print info on a setting'
        help_synopsis 'name'
        local -r name="$1"
        b_require_arg name "$name" || return 2
        local -r value="${_b_settings_values[$name]}"
        local -r default_value="${_b_settings_default_values[$name]}"
        local -r description="${_b_settings_descriptions[$name]}"
        local note="$description"
        [[ -n "$default_value" ]] && note="${note:+$note, }default: $default_value"
        printf '%s=%s%s\n' "$name" "$value" "${note:+ ($note)}"
    }

    b_settings_list() {
        help_summary 'print the registered settings'
        local name
        for name in "${!_b_settings_values[@]}"; do
            printf '%s\n' "$name"
        done
    }

    b_settings_register() {
        help_summary 'register a setting'
        help_synopsis 'name [default_value [description]]'
        local -r name="$1"
        b_require_arg name "$name" || return 2
        local -r default_value="$2"
        local -r description="$3"
        [[ -z "${_b_settings_values[$name]}" ]] && _b_settings_values[$name]="$default_value"
        _b_settings_default_values[$name]="$default_value"
        _b_settings_descriptions[$name]="$description"
    }

    b_settings_deregister() {
        help_summary 'deregister settings'
        help_synopsis 'name...'
        b_require_arg name "$1" || return 2
        local name
        for name in "$@"; do
            unset -v \
                '_b_settings_default_values[$name]' \
                '_b_settings_descriptions[$name]'
        done
    }

    b_settings_get() {
        help_summary 'print the value of a setting'
        help_synopsis 'name'
        local -r name="$1"
        b_require_arg name "$name" || return 2
        printf '%s\n' "${_b_settings_values[$name]:-${_b_settings_default_values[$name]}}"
    }

    b_settings_set() {
        help_summary 'set the value of a setting'
        help_synopsis 'name [value]'
        local -r name="$1"
        b_require_arg name "$name" || return 2
        local -r value="$2"
        _b_settings_values[$name]="$value"
    }

    b_settings_load() {
        help_summary 'load settings from file'
        help_synopsis '[file]'
        # shellcheck disable=SC2016
        help_description 'FILE defaults to the first "bish/settings" file found in $XDG_CONFIG_DIRS.'
        local file="$1"
        if [[ -n "$file" && ! -f "$file" ]]; then
            b_error "File $file not found"
            return 1
        fi
        [[ -z "$file" ]] && file="$(b_xdg_config_file bish/settings)" || return 0
        local line
        local name
        local value
        while read -r line; do
            # Skip comments and lines with no '='
            [[ "$line" == '#'* || "$line" != *'='* ]] && continue
            name="${line%%=*}"
            value="${line#*=}"
            _b_settings_values[$name]="$value"
        done < "$file"
    }

    b_settings_dump() {
        help_summary 'print the settings'
        help_description 'The settings are printed in a format compatible with b_settings_load.'
        for name in "${!_b_settings_values[@]}"; do
            printf '%s=%s\n' "$name" "${_b_settings_values[$name]}"
        done
    }

    _b_complete_settings_1() {
        local -r word="$2"
        if [[ "$COMP_CWORD" -eq 1 ]]; then
            mapfile -t COMPREPLY < <(compgen -W "$(b_settings_list)" -- "$word")
        fi
    }

    declare -Ag _b_settings_values=()
    declare -Ag _b_settings_default_values=()
    declare -Ag _b_settings_descriptions=()

    complete -F _b_complete_settings_1 b_settings_{info,get,set}

    b_settings_load
}

_b_settings_on_unload() {
    complete -r b_settings_{info,get,set}
    unset -f \
        b_settings_{info,list,register,deregister,get,set} \
        _b_complete_settings_1
    unset -v _b_settings_{values,default_values,descriptions}
}
